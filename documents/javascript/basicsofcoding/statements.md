---
menu: Programlama Temelleri
name: Kodlama
category: javacript
route: /javascript
---

Maxim Denisov. “You Don't Know JS.” kitabından alıntılar içermektedir.

## Kodlama

Bir bilgisayara neyi nasıl çalıştırması gerektiğini söylediğimiz bir grup yazı dizisine **_kaynak kod_** (source code) veya sadece **_kod_** diyoruz. Bu kaynak kodun referans ettiği şeyin kendisi de **_program_** oluyor.

Bir kodun nasıl yazılması gerektiğini belirten kurallara da **_yazılım dili_** (computer language) isimlendirmesi yapıyoruz. Çoğu zaman bu kurallarada **_syntax_** deniyor.

Bu kitapta bir şeyleri açıklarken çoğu zaman orjinal İngilizce metinleri kullanılacak. Çünkü yazılımın dili İngilizce ve bu dökümanları okuyanların da bu tanımlamalara aşina olmalarını arzu ediyorum.

## Statements

Bir programlama dilinde bir grup numara, text ve operatörden oluşan ve spesifik olarak bir işi yapan kümeye statement denir. JavaScript'de bir statement'da aşağı yukarı aşağıdaki gibi görünür.

```js
a = b * 2;
```

Buradaki **a** ve **b** karakterleri **_variable_** (değişken) olarak tanımlanır. Bir variable içinde kolayca bir değer tutabilirsiniz. İster bir sayı isterseniz bir text. Variables'ları bir değeri temsil eden sembolik tanımlama olarak düşünebilirsiniz.

Bunun dışında bu statement içinde bir de **2** değerini görüyorsunuz. Kendi başına herhangi bir değeri tutmayan sadece kendi değerini gösteriyor. O yüzden 2'ye **_literal value_** diyoruz.

**=** ve **\*** ifadelerine de **_operator_** diyoruz. Operatörler de değişken ve değerleri örnekteki gibi matematik veya mantıksal işlemlere sokar. Ve statementların çoğusu da örnekteki gibi **;** işaretiyle biter. JavaScript'te ; zorunlu değildir.

Örnekteki a = b \* 2 ifadesi bilgisayara şunu söyler, b temsil ettiği değeri al, 2 ile çarp, dönen değeri de a değişkeninde tut. Genel anlamda programlar bu gibi ifadelerin sıraya konup çalıştırılmasıdır.

## Expressions

Statement'lar yani ifadeler bir ya da daha fazla expression'lardan oluşur.

```js
a = b * 2;
```

Yine aynı örneğe bakacak olursak,

- ```2``` bir literal value expression
- ```b```, variable expression
- ```b * 2``` aritmetik expression
- ```a = b * 2``` assignment expression. ( Yani ```b * 2``` expression'ın sonucunu a değişkenine ata. )

Ayrıca bir diğer expression türü de, **call expression**. Fonksiyonları ifade eder.

## Programın Çalışması

Peki bu programlama ifadeleri bilgisayara neyi nasıl yapacağını nasıl söylüyor ?

a = b \* 2 gibi ifadeler, biz geliştiriciler için oldukça yardımcı oluyorlar. Böylece anlamlı biçimde bu ifadeleri okuyup yazabiliyoruz. Ama bu tarz yazım, bilgisayarın anlayacağı bir ifade değil.Bunun için bilgisayarın bileşenlerinden olan **_interpreter_** ve **_compiler_** bizim yazdığımız kodu bilgisayarın anlayacağı dile çevirerek bilgisayarın, kodumuzu anlamasını sağlıyor.

Bazı programlama dilleri, kodu **her execute edildiği anda** baştan sona doğru, satır satır çevirerek programın çalışmasını sağlar. Bu yaklaşıma **interpreting code** denir

Başka programlama dilleri de kodu çalışmadan önce bilgisayarın anlayacağı dile çevirir, program sonra o çevrilen kod üzerinden çalışır. interpreting yaklaşımındaki gibi kod her çalıştırılmaya çalışıldığında tekrar bir çeviri işleminden geçirilmez. Bu yaklaşıma da **compiling code** denir.

JavaScript, interpreting code yaklaşımına göre çalışır. Kod her çalıştırılmaya çalışıldığında, transpiler tarafından yorumlanıp öyle çalıştırılır.

Java gibi diller de kodun compile edilmiş yorumlanmış hali alınır deploy edilir. Çalıştığı sırada tekrar transpiler yani yorumlanma işleminden geçmez.

## Operatörler

Operatölerin değişken ve değerleri çeşitli işlemlerden geçirdiğini söylemiştik. Şimdi başka bir örneği inceleyelim.

```js
var a = 20;

a = a + 1;
a = a * 2;

console.log(a); // 42”
```

Buradaki işlemler sırasıyla yapılıyor. a değeri başlangıçta 20 değerini taşımaktayken, ilk önce +1 değerini ekleyip 21 değerini alıyor. Daha sonra a 21 değerini taşırken, 2 ile çarpılıyor ve sonunda kendisine 42 değeri atanıyor.

JavaScript'te bazı operatörler şöyle;

- **Matematik Opetatörler**: **+** (toplam), **-** (çıkarma), **\*** (çarpma), **/** (bölme)

- **Bileşik Operatörler**: **+=** , **-=**, **\*=** ve **/=** . Bileşik operatörler, matematik operatörleriyle atama opetatörlerini birleştirir. **a += 2** demek, **a = a + 2** ile aynı şeyi ifade eder.
- **Arttırma / Azaltma**: **++** ve **--** Burada da **a++** demek aslında **a = a + 1** demektir.
- **Object Property Access**: `.` Herhangi bir objenin bir property'sine erişmek için kullanılır. Örneğin sıkça kullandığımız **console** objesine içindeki **log** fonksiyonuna erişmek için **console.log()** şeklinde yazarız. Ayrıca **obj.a** diye de erişilebilen yöntemin alternatifi olarak **obj["a"]** şekli de kullanılabilir.
- **Eşitlik Operatörleri**: **==** (loose-eşitlik), **===** (strict eşitlik), **!=** (loose eşit değil), **!==** (strict eşit değil)
- **Karşılaştırma Operatörleri**: **<** (küçüktür), **>** (büyüktür), **<=** (loose küçük-eşit), **>=** (loose büyük eşit)
- **Mantıksal Opetatörler**: **&&** (ve), **||** (veya)

Burada yazılan loose ve strict ifadeleri type karşılaştırması ile ilgili. 43=="43" karşılaştırması yapıyorsak, iki eşittir ile yazdığımız loose eşittir ifadesi type kontrol etmez ve numara olan 43 ile, string olan 43'ü aynı kabul eder ve true döner. Ama 43 === "43" ifadesinde type'lar aynı olmadığından false dönecektir.

## TYPES

Program terminolojisinde bir değişkenin ne türde bir değer tutacağı type'lara göre belirlenir. JavaScript'te temel olarak type'lar şunlardır; **number, string** ve **boolean**

```js
"String"; //String
"Bu da string"; //String
58; //number
true; //booelan
false; //booelan
```

### Type'lar Arası Dönüşüm

Diyelim ki sizin bir number değişkeniniz var. Ama ekrana yazdırmak istiyorsunuz. Bunun için bu değeri string'e çevirip göstermeniz gerekir. JavaScript'te bu işleme **coercion** denir.

```js
var a = "42";
var b = Number(a);

console.log(a); // "42" String
console.log(b); // 42 Number
```

## Variables (Değişkenler)

Bazı programlama dillerinde bir değişken tanımı yapılırken spesifik olarak bir type verilir. Buna **static typing** denir. İstenmeyen type dönüşümünü engellemek ve programın tahmin edilebilirliliğini düşürmemesi için bu yöntem kullanılır.

JavaScript gibi dillerde static typing yoktur. Buna da **weak typing** veya **dynamic typing** denir. Bir değişken program akışına göre gerekirse number gerekirse farklı bir type'a sahip değeri tutabilir. Daha esnek kod yazmaya imkân sağlar.

```js
var amount = 99.99; // number değeri atandığı için başta amount type'ı number

amount = amount * 2;

console.log(amount); // 199.98

// `amount` değikenini string'e dönüştür
// ve başına "$" işaretini ekle
amount = "$" + String(amount); //amount artık bir string

console.log(amount); // "$199.98”
```

## Bloklar

Süslü parantez {...} içine yazılan kod grubuna bir kod bloğu diyoruz.

Genelde blokları, döngülerde, koşul ifadelerinde kullanıyoruz.

```js
var amount = 99.99;

// fiyat 10'dan büyük ise if bloğuna gir
if (amount > 10) {
  // <-- if bloğu başlıyor
  amount = amount * 2;
  console.log(amount); // 199.98
} // bir kod bloğunu bitirdiğimizde (;) kullanmıyoruz, dikkat edin.
```

## Koşullar

Koşullar kodlamamızdaki karar mekanizmalarıdır. Günlük hayatta kullandığımız, şu olursa bunu yapacağım, olmazsa bunu yapacağım demekten farkı yoktur.

```js
var bank_balance = 302.13;
var phone_amount = 99.99;

if (amount < bank_balance) {
  // banka hesabımdaki para, telefonun fiyatından büyük ise true döner ve if bloğuna girer. Değil ise else bloğuna girer.
  console.log("Telefon almaya param yetiyor, alayım");
} else {
  console.log("Bir sonraki modelin çıkmasını bekleyeyim");
}
```

## Döngüler

Döngüler de if blokları gibi belli koşula göre çalışır. Ancak if blokları bir kere çalışır, döngüler ise koşul doğru olduğu müddetçe tekrar tekrar çalışır.

```js
while (numOfCustomers > 0) {
  //müşteri sayısı 0'dan büyük olduğu müddetçe
  console.log("Nasıl yardımcı olabilirim ?");

  // müşterilere yardımcı ol

  numOfCustomers = numOfCustomers - 1; // her işlemden sonra müşteri sayısını bir azalt
}
```

While döngüsünün yanı sıra, do while, for gibi de çok kullanılan döngüler var. İlerleyen konulardan onlarada değinilecek.

## Fonksiyonlar

Programlar tek bir blok içine alt alta tekrar tekrar yazılmaz. Bu tekrar eden kod bloklarını isimlendirip ayrı bir yerde tutarız ve bunlara fonksiyon diyoruz. İhtiyaç olan yerde fonksiyonu ismiyle beraber çağırırız.

Mesela **printAmount** isimli bir fonksiyon kod içinde, çalışması için **()** paranteziyle çağrılır. **printAmount()** gibi

Ayrıca fonksiyonlar tanımlanması halinde parametre de alabilirler. Ve parametresiyle beraber çağrılabilir. **printAmount(30)** gibi

```js
function printAmount(amt) {
  // amt parametresini dışardan alabilir.
  console.log(amt.toFixed(2));
}

function formatAmount() {
  return "$" + amount.toFixed(2);
}

var amount = 99.99;

printAmount(amount * 2); // printAmount fonksiyonunu (amount*2) parametresi ile çağır. "199.98"

amount = formatAmount(); // formatAmount fonksiyonunu çağır
console.log(amount); // "$99.99”
```

## Scope

Diyelim ki bir telefon dükkânına gittiniz. Ve dükkan çalışanına almak istediğiniz telefon modelini söylediniz. O da istediğiniz modelin kendi dükkânlarında bulunmadığını ve size satamayacağını söyledi. Ancak dükkân envanterine bakıp size hangi dükkânda olabileceğini söylemeyi teklif etti. Siz eğer o telefonu almak istiyorsanız, başka bir dükkâna gitmek zorundasınız.

Programlamanın genel anlamda çalışma mantığıda bu örnekten ibaret. Bu konsepte biz **scope** adını veriyoruz ( teknik olarak **lexical scope** da diyebilirsiniz ).

JavaScript'te her fonksiyon kendi scope'una sahiptir. Scope temel anlamda, bir fonksiyonun içindeki değişkenlere nasıl erişileceğine ilişkin kurallardır. Bir fonksiyonun scope'unda yer alan değişkenlere sadece o fonksiyon içindeki kod erişebilir.

Bir değişken ismi aynı scope içinde unique (eşsiz) olmalıdır. Tek scope içinde aynı isimli iki fonksiyon tanımlanamaz. Farklı scope içinde tanımlanabilir.

```js
function one() {
  // buradaki a değişken one() fonksiyonuna ait
  var a = 1;
  console.log(a);
}

function two() {
  // buradaki a değişken two() fonksiyonuna ait
  var a = 2;
  console.log(a);
}

one(); // 1
two(); // 2
```

Ayrıca bilinmesi gereken en önemli şeylerden biri de, bir scope içinde başka bir scope tanımlanabilir. **Ve içteki scope dıştaki scope'un değişkenlerine erişebilir.**

```js
function outer() {
  var a = 1;

  function inner() {
    var b = 2;

    // içteki scope hem a hem de b ye erişebilir
    console.log(a + b); // 3
  }

  inner();

  // burada ise sadece a değişkenine erişebiliriz.
  console.log(a); // 1
}

outer();
```
