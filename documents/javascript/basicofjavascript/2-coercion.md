---
menu: JavaScript Temelleri
name: Coercion
category: javacript
route: /coercion
---

### Coercion

Coercion terimini type dönüşümü olarak tanımlayabiliriz. JavaScript'te iki tip coercion çeşiti var. Explicit Coercion ve Implicit Coercion.

Explicit Coercion'da korkacak ve kimsenin şikayet edeceği bir şey yok. Her dilde bu tip bir dönüşüm söz konusu zaten.

```js
//explicit coercion

var a = "42";

var b = Number(a);

a; // "42"
b; // 42 -- the number!
```

Ama Implicit Coercion yeni başlayanlar ve bu konuda tecrübe sahibi olmayanlar için sürpriz sonuçlar yaratabilir. Bu yüzden coercion belası, olmaz olsun böyle dil gibi tepkiler duyabilirsiniz, endişelenmeyin. Bilince kod yazarken bayağı bir avantaj bile sağlıyor.

```js
//implicit coercion

var a = "42";

var b = a * 1; // text olan "42", number 1 ile çarpıldığı için number 42'ye evrildi

a; // "42"
b; // 42 -- the number!
```
