---
menu: JavaScript Temelleri
name: Types
category: javacript
route: /types
---

## Types

JavaScript'te type'lar şöyle;

- string
- number
- null ve undefined
- object
- symbol ( ES6 )

JavaScript'te herhangi bir değişkenin type'ını belirlemek için `typeof` operatörü kullanılır.

```js
var a;
typeof a; // "undefined"

a = "hello world";
typeof a; // "string"

a = 42;
typeof a; // "number"

a = true;
typeof a; // "boolean"

a = null;
typeof a; // "object" -- ilginç değil mi :)

a = undefined;
typeof a; // "undefined"

a = { b: "c" };
typeof a; // "object”
```

typeof null'ın object olarak dönmesi bir bug. Ama bir sürü websitesi uygulama bu bug varken yazıldığı için çözülmüyor. Çünkü bu saatten sonra çözülmesi daha çok bug'a sebep olacak :)

## Object

JavaScript'teki en kullanışlı veri tipi, object. Object, birleşik biçimde bir sürü değeri tutar.

```js
var obj = {
  a: "hello world",
  b: 42,
  c: true
};

//dot notation ile objenin değerlerine erişme

obj.a; // "hello world"
obj.b; // 42
obj.c; // true

// bracket notation ile objenin değerlerine erişme

obj["a"]; // "hello world"
obj["b"]; // 42
obj["c"]; // true
```

## Arrays

JavaScript'te arrays (diziler) aslında yine objedir. Sadece properties/keys ikilisiyle değil, index'li olarak verileri tutar. Ve bu index diğer dillerdeki gibi 0 (sıfır)'dan başlar.

```js
var arr = ["hello world", 42, true];

arr[0]; // "hello world"
arr[1]; // 42
arr[2]; // true
arr.length; // 3 - array'in uzunluğu

typeof arr; // "object"
```

## Functions

Objenin bir diğer alt kümelerinden biri de function.

```js
function foo() {
  return 42;
}

foo.bar = "hello world"; //fonksiyon temelde bir obje ve biz böyle de atama yapabiliriz. Ama genelde yapılmaz, zorunlu kalmadıkça yapılmaması da iyidir.

typeof foo; // "function" main type.
typeof foo(); // "number"    fonksiyonun döndüğü değerin type'ı
typeof foo.bar; // "string”
```
