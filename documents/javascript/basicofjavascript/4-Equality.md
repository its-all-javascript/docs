---
menu: JavaScript Temelleri
name: Eşitlik
category: javacript
route: /equality
---

### Eşitlik

4 temel eşitlik opeatörümüz olduğundan bahsetmiştik. ==, ===, != ve !==. ( ! ) ünlem işareti gördüğünüz operatörler diğer operatörlerin simetrik olarak eşit olmadığı hali temsil ediyor. Ama eşitsizliğini değil. JavaScript'te **eşit olmama** ayrı bir kavram, **eşitsizlik** ayrı bir kavram.  

== iki eşittir ifadesi ile === üç eşittir ifadesi arasında temel fark ne diye sorsanız, genel geçer haliyle, == iki eşittir içeren ifade değerin eşitliğini kontrol ederken, === üç eşittirli ifade değer ve type kontrolü yapar denir. Doğru gibi olsa da tam olarak öyle değil. 

== iki eşittirli ifade, kontrolü yaparken coercion işlemine (type dönüşümüne) izin verir öyle kontrol yapar. === üç eşittirli ifade coercion işlemi olmadan karşılaştırma yapar. === üç eşittirli karşılaştırmaya bu yüzden **strict equality**'de denir

```js
var a = "42";
var b = 42;

a == b;            // true
a === b;        // false

```

Özel olarak bilmeniz gereken bir şey daha var. JavaScript'te temel type'lar haricinde olan array veya function gibi tipleri  == ve === operatörleriyle karşılaştırma yapılırken, referansları kontrol edilir. Tuttukları değerler değil. 

Örneğin bir array default olarak  her elemanın arasında ',' virgül konularak string'e coerced edilir. İki tane aynı elemana sahip array'i karşılaştırırsanız, değerin false çıkacağına dikkat ediniz. 

```js
var a = [1,2,3];
var b = [1,2,3];
var c = "1,2,3";

a == c;        // true
b == c;        // true
a == b;        // false
```

