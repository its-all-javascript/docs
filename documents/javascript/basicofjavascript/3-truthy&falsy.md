---
menu: JavaScript Temelleri
name: True/False
category: javacript
route: /trueorfalse
---

### Truthy & Falsy

Booelan olmayan bir değer coercion işleminden geçip booelan'a çevrilince true ya da false değerini alır. Bu yaklaşım en çok if statement'ı ile kullanılır.

**False dönen örnek değerler**

- "" (empty string)
- 0, -0, NaN (invalid number)
- null, undefined
- false

**True dönen örnek değerler**

- "hello"
- 42
- true
- [ ], `[1,"2",3]` ( arrays )
- { }, { a: 42 } ( objects )
- function foo( ) { ... } (functions)

Mesela aşağıdaki örnekte, text değeri empty string olduğu için if statement'ı içinde coercion'a uğrayıp, false değerine dönecek ve uygulama, if bloğuna girmeyecektir.

```js
var text = "";
if (text) {
  console.log("Merhaba" + text);
}
```
