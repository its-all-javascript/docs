---
menu: Style
name: FlexBox Style
category: react-native
route: /react-native-flexbox
---

import { Playground } from 'docz'
import { WebPlayer } from '../../../src/components/WebPlayer.js'

# FlexBox Style

CSS'de kullandığımız, `display:flex` ile benzer.

|                  |                           Options                           | Description                                                                                                                                                                                                  |
| ---------------- | :---------------------------------------------------------: | :----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `flexDirection`  |                        `row,column`                         | \(**column**\) seçerseniz elementleri dikey, \(**row**\) seçerseniz elementleri yatay sıralar. Bu seçim çok önemli çünkü bizim **primary axis** 'imiz ne olacak onu belirler. Diğeri **secondary axis** olur |
| `justifyContent` | `flex-start, center, flex-end, space-around, space-between` | Child elementlerimiz **primary axis** boyunca Container içinde nasıl dağılsın.                                                                                                                               |
| `alignItems`     |           `flex-start, center, flex-end,stretch`            | Child elementlerimiz **secondary axis** boyunca Container içinde nasıl dağılsın.                                                                                                                             |

```javascript
import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});
```

React Native de style tanımlamalarını, kendi kütüphanesinin içinde bulunan **StyleSheet** ile yapıyoruz. Bilindik bir obje tanımlaması yapıyoruz aslında. Yukarıdaki örnekte tanımladığımız **container** objesini aşağıdaki gibi kullanıyoruz.

<Playground>
  <WebPlayer title="FlexBox1" style={{height:500}} height={500} code={`
    import React from 'react';
    import { View, StyleSheet, AppRegistry } from 'react-native';
    \n
    const App = () => {
      return <View style={styles.container} />
    };
    \n
    const styles = StyleSheet.create({
      container: {
      flex:1,
      backgroundColor:'red'
    }
    });
\n
    AppRegistry.registerComponent('App', () => App);
`}/>
</Playground>

---

Şimdi container içine kırmızı ve mavi renklere sahip iki tane layout View ekleyelim. Aşağıda görüldüğü üzere flexBox'da default flexDirection column olduğu için iki elemanı alt alta hizalıyor.

<Playground>
  <WebPlayer title="FlexBox2" style={{height:500}} height={500} code={`
    import React from 'react';
    import { View, StyleSheet, AppRegistry } from 'react-native';
    \n
    const App = () => {
      return (
        <View style={styles.container}>
          <View style={styles.layout1} />
          <View style={styles.layout2} />
        </View>
      )
    };
    \n
    const styles = StyleSheet.create({
      container: {
       flex:1,
      },
      layout1: {
       flex:1,
       backgroundColor: 'red',
      },
      layout2: {
       flex:1,
       backgroundColor: 'blue',
      },
    });
\n
    AppRegistry.registerComponent('App', () => App);
`}/>
</Playground>

---

flexDirection değerine row değerini verirsek, container componentinin içindeki elementleri yan yana hizaladığını görürüz.

<Playground>
  <WebPlayer title="FlexBox3" style={{height:500}} height={500} code={`
    import React from 'react';
    import { View, StyleSheet, AppRegistry } from 'react-native';
    \n
    const App = () => {
      return (
        <View style={styles.container}>
          <View style={styles.layout1} />
          <View style={styles.layout2} />
        </View>
      )
    };
    \n
    const styles = StyleSheet.create({
      container: {
       flex:1,
       flexDirection: 'row'
      },
      layout1: {
       flex:1,
       backgroundColor: 'red',
      },
      layout2: {
       flex:1,
       backgroundColor: 'blue',
      },
    });
\n
    AppRegistry.registerComponent('App', () => App);
`}/>
</Playground>

---

Kırmızı ve Mavi layout componentlerin her ikisi de üstteki örneklerde flex:1 değerini almakta. Dolayısıyla kendilerine verilen alanı eşit biçimde paylaşıyorlar. Eğer Mavi component'e kırmızının iki katı olacak biçinde flex değerini verirsek, tüm alanın 2/3 ünü Mavi, 1/3 ünü Kırmızı component kullanacak. Siz aşağıda kırmızıya 4, maviye 8 verseniz de oransal olarak yine aynı şekilde bölüşecekler.

<Playground>
  <WebPlayer title="FlexBox4" style={{height:500}} height={500} code={`
    import React from 'react';
    import { View, StyleSheet, AppRegistry } from 'react-native';
    \n
    const App = () => {
      return (
        <View style={styles.container}>
          <View style={styles.layout1} />
          <View style={styles.layout2} />
        </View>
      )
    };
    \n
    const styles = StyleSheet.create({
      container: {
       flex:1,
       flexDirection: 'row'
      },
      layout1: {
       flex:1,
       backgroundColor: 'red',
      },
      layout2: {
       flex:2,
       backgroundColor: 'blue',
      },
    });
\n
    AppRegistry.registerComponent('App', () => App);
`}/>
</Playground>

---

Şimdi container componentin içine 3 tane kırmızı, mavi ve siyah renkli top koyalım.

<Playground>
  <WebPlayer title="FlexBox5" style={{height:500}} height={500} code={`
    import React from 'react';
    import { View, StyleSheet, AppRegistry } from 'react-native';
    \n
    const App = () => {
      return (
        <View style={styles.container}>
          <View style={styles.circle1} />
          <View style={styles.circle2} />
          <View style={styles.circle3} />
        </View>
      )
    };
    \n
    const styles = StyleSheet.create({
      container: {
       flex:1,
      },
      circle1: {
       width: 40,
       height: 40,
       borderRadius: 80,
       backgroundColor: 'red',
      },
      circle2: {
       width: 40,
       height: 40,
       borderRadius: 80,
       backgroundColor: 'blue',
      },
      circle3: {
       width: 40,
       height: 40,
       borderRadius: 80,
       backgroundColor: 'black',
      },
    });
\n
    AppRegistry.registerComponent('App', () => App);
`}/>
</Playground>

---

container componentin flexDirection default değeri column olduğu için üstteki örnekte toplar alt alta sıralanıyordu. Şimdi flexDirection değerimizi row yapalım. Yan yana sıralandıklarını göreceğiz.

<Playground>
  <WebPlayer title="FlexBox6" style={{height:500}} height={500} code={`
    import React from 'react';
    import { View, StyleSheet, AppRegistry } from 'react-native';
    \n
    const App = () => {
      return (
        <View style={styles.container}>
          <View style={styles.circle1} />
          <View style={styles.circle2} />
          <View style={styles.circle3} />
        </View>
      )
    };
    \n
    const styles = StyleSheet.create({
      container: {
       flex:1,
       flexDirection: 'row',
      },
      circle1: {
       width: 40,
       height: 40,
       borderRadius: 80,
       backgroundColor: 'red',
      },
      circle2: {
       width: 40,
       height: 40,
       borderRadius: 80,
       backgroundColor: 'blue',
      },
      circle3: {
       width: 40,
       height: 40,
       borderRadius: 80,
       backgroundColor: 'black',
      },
    });
\n
    AppRegistry.registerComponent('App', () => App);
`}/>
</Playground>

---

flexDirection'ı row yaptığımız için primary-axis yatay oldu. Primary axis boyunca elementleri konumlandıran fonksiyonumuz justifyContent oldu. (Sayfa başındaki tabloyu kontrol edebilirsiniz.). justifyContent'in alabileceği değerler flex-start, center, flex-end, space-around, space-between. default değeri flex-start. Bir önceki örnekte default halini görüyoruz. Şimdi elementleri birbirlerine eşit uzaklıkta olacak biçimde dağıtalım. Bunun için de justifyContent'e space-around değeri vermemiz yeterli.

<Playground>
  <WebPlayer title="FlexBox7" style={{height:500}} height={500} code={`
    import React from 'react';
    import { View, StyleSheet, AppRegistry } from 'react-native';
    \n
    const App = () => {
      return (
        <View style={styles.container}>
          <View style={styles.circle1} />
          <View style={styles.circle2} />
          <View style={styles.circle3} />
        </View>
      )
    };
    \n
    const styles = StyleSheet.create({
      container: {
       flex:1,
       flexDirection: 'row',
       justifyContent: 'space-around',
      },
      circle1: {
       width: 40,
       height: 40,
       borderRadius: 80,
       backgroundColor: 'red',
      },
      circle2: {
       width: 40,
       height: 40,
       borderRadius: 80,
       backgroundColor: 'blue',
      },
      circle3: {
       width: 40,
       height: 40,
       borderRadius: 80,
       backgroundColor: 'black',
      },
    });
\n
    AppRegistry.registerComponent('App', () => App);
`}/>
</Playground>

---

primary-axis yatay olduğu için, seconday-axis dikey. secondary-axis'de hizalamak için kullanacağımız kodda alignItems. alignItems'da flex-start, center, flex-end,stretch değerlerini almakta. Default değeri tabiki flex-start. Eğer biz seconday-axis boyunca elementleri merkezde başlayarak hizalamak istiyorsak, aşağıdaki gibi alignItems'a center değerini verebiliriz.

<Playground>
  <WebPlayer title="FlexBox8" style={{height:500}} height={500} code={`
    import React from 'react';
    import { View, StyleSheet, AppRegistry } from 'react-native';
    \n
    const App = () => {
      return (
        <View style={styles.container}>
          <View style={styles.circle1} />
          <View style={styles.circle2} />
          <View style={styles.circle3} />
        </View>
      )
    };
    \n
    const styles = StyleSheet.create({
      container: {
       flex:1,
       flexDirection: 'row',
       justifyContent: 'space-around',
       alignItems: 'center'
      },
      circle1: {
       width: 40,
       height: 40,
       borderRadius: 80,
       backgroundColor: 'red',
      },
      circle2: {
       width: 40,
       height: 40,
       borderRadius: 80,
       backgroundColor: 'blue',
      },
      circle3: {
       width: 40,
       height: 40,
       borderRadius: 80,
       backgroundColor: 'black',
      },
    });
\n
    AppRegistry.registerComponent('App', () => App);
`}/>
</Playground>

---

Bir de alignSelf methodumuz. Yukarıdaki örnekten devam edersek, tüm elementlerimizi merkezde toplamıştık. alignSelf komutu ile spesifik olarak bir elementi diğerlerinden farklı olarak hizalayabiliriz. Aşağıdaki gibi siyah top'un alignSelf değerine flex-start verelim. Gördüğünüz gibi siyah top merkezde değil, dikey düzlemde en üstte hizalandı.

<Playground>
  <WebPlayer title="FlexBox9" style={{height:500}} height={500} code={`
    import React from 'react';
    import { View, StyleSheet, AppRegistry } from 'react-native';
    \n
    const App = () => {
      return (
        <View style={styles.container}>
          <View style={styles.circle1} />
          <View style={styles.circle2} />
          <View style={styles.circle3} />
        </View>
      )
    };
    \n
    const styles = StyleSheet.create({
      container: {
       flex:1,
       flexDirection: 'row',
       justifyContent: 'space-around',
       alignItems: 'center'
      },
      circle1: {
       width: 40,
       height: 40,
       borderRadius: 80,
       backgroundColor: 'red',
      },
      circle2: {
       width: 40,
       height: 40,
       borderRadius: 80,
       backgroundColor: 'blue',
      },
      circle3: {
       width: 40,
       height: 40,
       borderRadius: 80,
       backgroundColor: 'black',
       alignSelf : 'flex-start'
      },
    });
\n
    AppRegistry.registerComponent('App', () => App);
`}/>
</Playground>

---

Şimdi flexDirection'ı row olan ve içinde 10 tane top olan contaier component'i inceleyelim. Bir şeylerin ters gittiğini görüyor olmalısınız.

<Playground>
  <WebPlayer title="FlexBox10" style={{height:500}} height={500} code={`
    import React from 'react';
    import { View, StyleSheet, AppRegistry } from 'react-native';
    \n
    const App = () => {
      return (
        <View style={styles.container}>
          {Array.from(Array(10)).map((item)=> <View style={styles.circle} /> ) }
        </View>
      )
    };
    \n
    const styles = StyleSheet.create({
      container: {
       flex:1,
       flexDirection: 'row',
      },
      circle: {
       width: 40,
       height: 40,
       borderRadius: 80,
       backgroundColor: 'red',
       margin: 7,
      },
    });
\n
    AppRegistry.registerComponent('App', () => App);
`}/>
</Playground>

---

Container component'e bir taşma söz konusu ise buna çare olarak flexWrap değerini kullanabiliriz. flexWrap default olarak nowrap değerini almakta. Eğer biz aşağıdaki gibi flexWrap key'ine wrap değerini verirsek bir satır dolduğunda alt satırdan devam ederek elementleri yerleştir diyebiliriz.

<Playground>
  <WebPlayer title="FlexBox11" style={{height:500}} height={500} code={`
    import React from 'react';
    import { View, StyleSheet, AppRegistry } from 'react-native';
    \n
    const App = () => {
      return (
        <View style={styles.container}>
          {Array.from(Array(10)).map((item)=> <View style={styles.circle} /> ) }
        </View>
      )
    };
    \n
    const styles = StyleSheet.create({
      container: {
       flex:1,
       flexDirection: 'row',
       flexWrap: 'wrap'
      },
      circle: {
       width: 40,
       height: 40,
       borderRadius: 80,
       backgroundColor: 'red',
       margin: 7,
      },
    });
\n
    AppRegistry.registerComponent('App', () => App);
`}/>
</Playground>



------

Ayrıca flexBox ile ilgili daha fazla pratik yapmak isterseniz, [şuradaki](https://flexboxfroggy.com/) kurbağa oyununu veya [buradaki](http://www.flexboxdefense.com/)  başka bir oyunu oynayabilirsiniz. Oradaki seviyeleri bitirdiğinizde konunun zihninizde bayağı bir pekişeceğine inanıyorum. Sadece bilmeniz gereken nokta web'de flexDirection default değeri row, react-native style sisteminde column. Dolayısıyla başlangıçta primary-axis değerleri farklı. Ayrıca react-native'de web'den farklı olarak style isimlendirmeleri camelCase kullanarak yazılmakta.

