---
menu: Başlangıç
name: React Native Nedir
category: react-native
route: /react-native
---

# React Native Nedir

React Native, React ekosistemini kullanarak Javascript ile gerçek anlamda native IOS ve Android uygulamalar geliştirebileceğiniz bir kütüphane.

Fast Refresh sayesinde native geliştirme yapmaya göre çok daha hızlı ve pratik. Hem Android hem de IOS için aynı codebase kullanılmasından dolayı zaman ve bütçe tasarrufu sağlaması açısından daha ekonomik.

Aşağıdaki yapıyı bilmenizde fayda var. React Native'de, native uygulama geliştirmeye imkân sağlayan şey araya konan **React Native Bridge yapısı**. Bu yapı sayesinde Javascript olarak yazdığımız bir <View/> komponenti, Bridge yani köprü içinden geçerken, çalıştığı platforma göre; IOS ise UIView, Android ise android.view.View olarak, native bir biçimde derlenmektedir. Özet olarak React Native Bridge yapısı bu. Siz de ihtiyaç halinde native IOS ve Android geliştirmelerinizi bu kısıma yazıp native geliştirme yapabilirsiniz. React Native sizi sadece kendi ekosistemi ile asla sınırlandırmıyor. İsterseniz var olan native uygulamanızın içine de React Native ile kod yazabilirsiniz.

![./images/reactnativebridge.png](./images/reactnativebridge.png)

Açıkçası artık tonla React Native Nedir başlıklı Türkçe yazı olduğundan bununla ilgili daha fazla şey yazmak istemiyorum.

Katkıda bulunmak, ben de şu konuyu anlatmak istiyorum derseniz şuraya mail atabilirsiniz [ysfzrn@gmail.com](https://github.com/ysfzrn/react-native-turkce/tree/3539da58d5b1e4a06f16d3564238433117e57fd6/ysfzrn@gmail.com)

veya

[**buradaki**](https://gitlab.com/its-all-javascript/docs) repo'ya pull request gönderebilirsiniz.

**Emeği geçenler**

- Yusuf Zeren
- [Haydar Şahin](https://twitter.com/haydarsahinblog) \( Gnu/Linux Kurulumu \)
