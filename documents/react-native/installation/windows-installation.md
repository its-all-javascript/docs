---
menu: Kurulum
name: Windows Kurulumu
category: react-native
route: /react-native-windows-kurulum
---

# Windows Kurulumu

## 1-Node Js kurulumu

[Node JS](https://nodejs.org/en/download/) kurulumunu kendi sayfası üzerinden klasik olarak next next diyerek kurabilirsiniz.

![./images/import.png](./images/import.png)

## 2-Android Studio Kurulumu

[Android Studio](https://developer.android.com/studio/install.html) kurulum dosyasını indirip, kuralım. Android SDK nin nereye yükleneceği bilgisi önemli, aklımızda bulunsun.

![./images/import2.png](./images/import2.png)

Kurulumu yaparken bir seçim ekranı gelecek, burada “Custom Installation” seçeneğini seçmelisiniz.

![./images/import3.png](./images/import3.png)

Yükleme sırasında ya da zaten bilgisayarınızda yüklü bir Android Studio var ise aşağıdaki listedeki araçların yüklendiğinden emin olun. Yoksa “SDK Components Setup” ekranından yükleyin.

- - Android SDK
  - Android SDK Platform
  - Performance \(Intel HAXM\)
  - Android Virtual Device

  ![./images/import5.png](./images/import5.png)

Yukarıda ki resimde görüldüğü gibi “Android Studio Installation” otamatik olarak bilgisayarınıza Android’in en son sürümünü\(şu an için Android 7.1.1 Nougat\) kuracaktır. Ancak React-Native, Android 6.0 \(Marshmallow\) sürümünü istemektedir. Bunun için Android Studio’nun açılış sayfasında “Configure” butonuna tıklayıp, Ayarlar bölümünde “Android SDK” yı işaretleyip, buradan Marshmallow sürümünü kuralım.

![./images/import6.png](./images/import6.png)

Marshmallow sürümünü kurarken yukarıda işaretli araçları seçmeyi unutmayın. Eğer bu araçları göremiyorsanız, ekranın sağ alt köşesindeki “Show Package Details” seçeneğini işaretleyerek paket detaylarında görebilirsiniz. Apply butonuna basıp, tekrar next next diye devam ediyoruz.

Şimdi Sistem Değişkenlerine “**ANDROID_HOME**” değişkenimizi ekleyelim.

![./images/import7.png](./images/import7.png)

ANDROID_HOME değişkenini ekledikten sonra PATH değişkenine aşağıdaki satırı kopyalayıp yapıştırın. \(

_Tahminim en çok insanların gözünden kaçırdığı nokta burası oluyor, ortam değişkenlerini eklemeye, harf, noktalı virgül atlamamaya çok dikkat edin_\) `;%ANDROID_HOME%\tools;%ANDROID_HOME%\platform-tools`

![./images/import8.png](./images/import8.png)

## 3-Android Emulator Kurulumu

Android Studio aşağıdaki gibi AVD Manager'ı açalım.

![./images/import9.png](./images/import9.png)

AVD Manager içinde alttaki Create Virtual Device ile kendinize göre bir emulator seçip kolay bir şekilde yükleyebilirsiniz.

Daha sonra yüklenilen emulatorler aşağıdaki gibi listelenecek. Oradaki play butonuna basıp, emulator'u çalıştırabilirsiniz.

![./images/import10.png](./images/import10.png)

## 4-React-Native CLI Kurulumu

İşin zor kısmını atlattık. Şimdi Javascript dünyasına nihayet girebiliriz. İlk olarak komut satırına aşağıdaki kodu yazarak “react-native-cli” ‘ı global olarak yükleyelim.

`npm install -g react-native-cli`

![./images/import13.png](./images/import13.png)

Sonra ilk react-native projemizi kendi oluşturduğumuz bir workspace klasöründe aşağıdaki npm komutu ile yaratalım.

`react-native init helloNative`

![./images/import15.png](./images/import15.png)

Emulator'umuzu çalıştıralım ve proje klasörümüze komut satırıyla girip aşağıdaki kod ile projemizi sanal cihazımızda çalıştıralım.

`react-native run-android`

![./images/import16.png](./images/import16.png)

**Hata :** Bilgisayarımız da JAVA ve JDK yüklü değil.

**Çözüm:** [Şuradaki](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) adresten son sürüm JDK yı indirebilirsiniz.

JDK’yı kurduktan sonra, tekrar react-native run-android komutu ile projemizi çalıştırmayı deneyelim.

![./images/import17.png](./images/import17.png)

**Hata:** Build Tools 23.0.1 i yüklemememişiz.

**Çözüm:** Komut satırına “android” komutu yazarak SDK manager’i açıp, eksik olan tool’u \(Build Tools 23.0.1\) kuralım.

![./images/import20.png](./images/import20.png)

Evet son sorunumuzu da çözdükten sonra tekrar **react-native run-android** komutunu çalıştıralım.

Ve nihayet çalıştırdık.

![./images/import22.png](./images/import22.png)

_Not: React Native versiyonu olarak bu yazının yazıldığı an itibariye son sürüm olarak, 0.39.2 yi kurduk._
