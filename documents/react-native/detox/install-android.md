---
menu: Detox
name: Detox Setup Android
category: react-native
route: /detox-setupi-android
---

## DETOX SETUP - ANDROID

> Sisteminizde yüklü kullanabileceğiniz emulatorlerin listesini görmek için şu kodu kullanabilirsiniz

```sh
emulator -list-avds
```
