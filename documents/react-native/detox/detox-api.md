---
menu: Detox
name: Detox API
category: react-native
route: /detox-api
---

# DETOX API

## detox objesi

- [detox.init()](https://github.com/wix/Detox/blob/master/docs/APIRef.DetoxObjectAPI.md#detoxinit)

  ```jsx
  const config = require("../package.json").detox;

  before(async () => {
    await detox.init(config);
  });
  ```

- [detox.beforeEach()](https://github.com/wix/Detox/blob/master/docs/APIRef.DetoxObjectAPI.md#detoxbeforeeach)

- [detox.afterEach()](https://github.com/wix/Detox/blob/master/docs/APIRef.DetoxObjectAPI.md#detoxaftereach)

- [detox.cleanup()](https://github.com/wix/Detox/blob/master/docs/APIRef.DetoxObjectAPI.md#detoxcleanup)

---

## device objesi

- [`device.launchApp()`](https://github.com/wix/Detox/blob/master/docs/APIRef.DeviceObjectAPI.md#devicelaunchappparams)
- [`device.terminateApp()`](https://github.com/wix/Detox/blob/master/docs/APIRef.DeviceObjectAPI.md#deviceterminateapp)
- [`device.sendToHome()`](https://github.com/wix/Detox/blob/master/docs/APIRef.DeviceObjectAPI.md#devicesendtohome)
- [`device.reloadReactNative()`](https://github.com/wix/Detox/blob/master/docs/APIRef.DeviceObjectAPI.md#devicereloadreactnative)
- [`device.installApp()`](https://github.com/wix/Detox/blob/master/docs/APIRef.DeviceObjectAPI.md#deviceinstallapp)
- [`device.uninstallApp()`](https://github.com/wix/Detox/blob/master/docs/APIRef.DeviceObjectAPI.md#deviceuninstallapp)
- [`device.openURL(url)`](https://github.com/wix/Detox/blob/master/docs/APIRef.DeviceObjectAPI.md#deviceopenurlurl-sourceappoptional)
- [`device.sendUserNotification(params)`](https://github.com/wix/Detox/blob/master/docs/APIRef.DeviceObjectAPI.md#devicesendusernotificationparams)
- [`device.sendUserActivity(params)`](https://github.com/wix/Detox/blob/master/docs/APIRef.DeviceObjectAPI.md#devicesenduseractivityparams)
- [`device.setOrientation(orientation)`](https://github.com/wix/Detox/blob/master/docs/APIRef.DeviceObjectAPI.md#devicesetorientationorientation)
- [`device.setLocation(lat, lon)`](https://github.com/wix/Detox/blob/master/docs/APIRef.DeviceObjectAPI.md#devicesetlocationlat-lon)
- [device.setURLBlacklist](https://github.com/wix/Detox/blob/master/docs/APIRef.DeviceObjectAPI.md#deviceseturlblacklisturls)
- [`device.enableSynchronization()`](https://github.com/wix/Detox/blob/master/docs/APIRef.DeviceObjectAPI.md#deviceenablesynchronization)
- [`device.disableSynchronization()`](https://github.com/wix/Detox/blob/master/docs/APIRef.DeviceObjectAPI.md#devicedisablesynchronization)
- [`device.resetContentAndSettings()` **iOS Only**](https://github.com/wix/Detox/blob/master/docs/APIRef.DeviceObjectAPI.md#deviceresetcontentandsettings-ios-only)
- [`device.getPlatform()`](https://github.com/wix/Detox/blob/master/docs/APIRef.DeviceObjectAPI.md#devicegetplatform)
- [`device.takeScreenshot(name)`](https://github.com/wix/Detox/blob/master/docs/APIRef.DeviceObjectAPI.md#devicetakescreenshotname)
- [`device.shake()` **iOS Only**](https://github.com/wix/Detox/blob/master/docs/APIRef.DeviceObjectAPI.md#deviceshake-ios-only)
- [`device.setBiometricEnrollment(bool)` **iOS Only**](https://github.com/wix/Detox/blob/master/docs/APIRef.DeviceObjectAPI.md#devicesetbiometricenrollmentbool-ios-only)
- [`device.matchFace()` **iOS Only**](https://github.com/wix/Detox/blob/master/docs/APIRef.DeviceObjectAPI.md#devicematchface-ios-only)
- [`device.unmatchFace()` **iOS Only**](https://github.com/wix/Detox/blob/master/docs/APIRef.DeviceObjectAPI.md#deviceunmatchface-ios-only)
- [`device.matchFinger()` **iOS Only**](https://github.com/wix/Detox/blob/master/docs/APIRef.DeviceObjectAPI.md#devicematchfinger-ios-only)
- [`device.unmatchFinger()` **iOS Only**](https://github.com/wix/Detox/blob/master/docs/APIRef.DeviceObjectAPI.md#deviceunmatchfinger-ios-only)
- [`device.clearKeychain()` **iOS Only**](https://github.com/wix/Detox/blob/master/docs/APIRef.DeviceObjectAPI.md#deviceclearkeychain-ios-only)
- [`device.setStatusBar()` **iOS Only**](https://github.com/wix/Detox/blob/master/docs/APIRef.DeviceObjectAPI.md#devicesetstatusbar-ios-only)
- [`device.resetStatusBar()` **iOS Only**](https://github.com/wix/Detox/blob/master/docs/APIRef.DeviceObjectAPI.md#deviceresetstatusbar-ios-only)
- [`device.reverseTcpPort()` **Android Only**](https://github.com/wix/Detox/blob/master/docs/APIRef.DeviceObjectAPI.md#devicereversetcpport-android-only)
- [`device.unreverseTcpPort()` **Android Only**](https://github.com/wix/Detox/blob/master/docs/APIRef.DeviceObjectAPI.md#deviceunreversetcpport-android-only)
- [`device.pressBack()` **Android Only**](https://github.com/wix/Detox/blob/master/docs/APIRef.DeviceObjectAPI.md#devicepressback-android-only)
- [`device.getUIDevice()` **Android Only**](https://github.com/wix/Detox/blob/master/docs/APIRef.DeviceObjectAPI.md#devicegetuidevice-android-only)

---

## LifeCycle Methodlar

### beforeEach ve afterEach

```js
beforeEach(async function() {
  testSummary = {
    title: this.currentTest.title,
    fullName: this.currentTest.fullTitle(),
    status: "running"
  };

  await detox.beforeEach(testSummary);
});

afterEach(async function() {
  testSummary.status = this.currentTest.state || "failed";
  await detox.afterEach(testSummary);
});
```

### after

```js
after(async () => {
  await detox.cleanup();
});
```

---

## Matchers

- [`by.id()`](https://github.com/wix/Detox/blob/master/docs/APIRef.Matchers.md#byidid)
- [`by.text()`](https://github.com/wix/Detox/blob/master/docs/APIRef.Matchers.md#bytexttext)
- [`by.label()`](https://github.com/wix/Detox/blob/master/docs/APIRef.Matchers.md#bylabellabel)
- [`by.type()`](https://github.com/wix/Detox/blob/master/docs/APIRef.Matchers.md#bytypenativeviewtype)
- [`by.traits()`](https://github.com/wix/Detox/blob/master/docs/APIRef.Matchers.md#bytraitstraits) (iOS only)

### Multiple Matchers

```js
element(by.id("uniqueId").and(by.text("some text")));
```

testID'si child olan bir elementin parent'ı ile ilgili işlem yapmak istiyorsak;\*

```js
element(by.id("child").withAncestor(by.id("parent")));
```

_ya da tam tersi;_

```js
element(by.id("parent").withDescendant(by.id("child")));
```

> Diyelim ki bir listedeki elemanlardan birini seçmek istiyorsanız `testID={'listitem' + this.props.index}` gibi bir kullanım yapabilirsiniz

##### TIP: Finding the back button on iOS

iOS 11:

```
element(by.traits(['button']).and(by.label('Back')));
```

iOS 10:

```
element(by.type('_UIModernBarButton').and(by.label('Back')));
```

---

## Actions

- [`.tap()`](https://github.com/wix/Detox/blob/master/docs/APIRef.ActionsOnElement.md#tap)

  ```js
  await element(by.id("tappable")).tap();
  ```

- [`.longPress(duration)`](https://github.com/wix/Detox/blob/master/docs/APIRef.ActionsOnElement.md#longpress)

  _duration parametresi IOS için geçerli_

  ```js
  await element(by.id("tappable")).longPress();
  ```

- [`.multiTap(times)`](https://github.com/wix/Detox/blob/master/docs/APIRef.ActionsOnElement.md#multitaptimes)

  ```js
  await element(by.id("tappable")).multiTap(3);
  ```

- [`.tapAtPoint()`](https://github.com/wix/Detox/blob/master/docs/APIRef.ActionsOnElement.md#tapatpoint)

  ```js
  await element(by.id("tappable")).tapAtPoint({ x: 5, y: 10 });
  ```

- [`.tapBackspaceKey()`](https://github.com/wix/Detox/blob/master/docs/APIRef.ActionsOnElement.md#tapbackspacekey)

  ```js
  await element(by.id("textField")).tapBackspaceKey();
  ```

- [`.tapReturnKey()`](https://github.com/wix/Detox/blob/master/docs/APIRef.ActionsOnElement.md#tapreturnkey)

  ```js
  await element(by.id("textField")).tapReturnKey();
  ```

- [`.typeText()`](https://github.com/wix/Detox/blob/master/docs/APIRef.ActionsOnElement.md#typetexttext)

  ```js
  await element(by.id("textField")).typeText("passcode");
  ```

- [`.replaceText()`](https://github.com/wix/Detox/blob/master/docs/APIRef.ActionsOnElement.md#replacetexttext)

  ```js
  await element(by.id("textField")).replaceText("passcode again");
  ```

- [`.clearText()`](https://github.com/wix/Detox/blob/master/docs/APIRef.ActionsOnElement.md#cleartext)

  ```js
  await element(by.id("textField")).clearText();
  ```

- [`.scroll(pixels, direction, startPositionX=NaN, startPositionY=NaN)`](https://github.com/wix/Detox/blob/master/docs/APIRef.ActionsOnElement.md#scrollpixels-direction-startpositionxnan-startpositionynan)

  ```js
  await element(by.id("scrollView")).scroll(100, "down", NaN, 0.85);
  await element(by.id("scrollView")).scroll(100, "up");
  ```

- [`.scrollTo(edge)`](https://github.com/wix/Detox/blob/master/docs/APIRef.ActionsOnElement.md#scrolltoedge)

  ```js
  await element(by.id("scrollView")).scrollTo("bottom");
  await element(by.id("scrollView")).scrollTo("top");
  ```

- [`.swipe(direction, speed, percentage)`](https://github.com/wix/Detox/blob/master/docs/APIRef.ActionsOnElement.md#swipedirection-speed-percentage)

  ```js
  await element(by.id("scrollView")).swipe("down");
  await element(by.id("scrollView")).swipe("down", "fast");
  await element(by.id("scrollView")).swipe("down", "fast", 0.5);
  ```

- [`.setColumnToValue(column, value)`](https://github.com/wix/Detox/blob/master/docs/APIRef.ActionsOnElement.md#setcolumntovaluecolumn-value--ios-only) **iOS only**

  ```js
  await expect(element(by.id("pickerView"))).toBeVisible();
  await element(by.id("pickerView")).setColumnToValue(1, "6");
  await element(by.id("pickerView")).setColumnToValue(2, "34");
  ```

- [`.pinchWithAngle(direction, speed, angle)`](https://github.com/wix/Detox/blob/master/docs/APIRef.ActionsOnElement.md#pinchwithangledirection-speed-angle--ios-only) **iOS only**

  ```js
  await expect(element(by.id("PinchableScrollView"))).toBeVisible();
  await element(by.id("PinchableScrollView")).pinchWithAngle(
    "outward",
    "slow",
    0
  );
  ```

- [`.setDatePickerDate(dateString, dateFormat)`](https://github.com/wix/Detox/blob/master/docs/APIRef.ActionsOnElement.md#setdatepickerdatedatestring-dateformat--ios-only) **iOS only**

  ```js
  await expect(element(by.id("datePicker"))).toBeVisible();
  await element(by.id("datePicker")).setDatePickerDate(
    "2019-02-06T05:10:00-08:00",
    "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
  );
  ```

---

## Expectations

- [`.toBeVisible()`](https://github.com/wix/Detox/blob/master/docs/APIRef.Expect.md#tobevisible)

  ```js
  await expect(element(by.id("UniqueId204"))).toBeVisible();
  ```

- [`.toBeNotVisible()`](https://github.com/wix/Detox/blob/master/docs/APIRef.Expect.md#tobenotvisible)

  ```js
  await expect(element(by.id("UniqueId205"))).toBeNotVisible();
  ```

- [`.toExist()`](https://github.com/wix/Detox/blob/master/docs/APIRef.Expect.md#toexist)

  ```js
  await expect(element(by.id("UniqueId205"))).toExist();
  ```

- [`.toNotExist()`](https://github.com/wix/Detox/blob/master/docs/APIRef.Expect.md#tonotexist)

  ```js
  await expect(element(by.id("RandomJunk959"))).toNotExist();
  ```

- [`.toHaveText()`](https://github.com/wix/Detox/blob/master/docs/APIRef.Expect.md#tohavetexttext)

  ```js
  await expect(element(by.id("UniqueId204"))).toHaveText("I contain some text");
  ```

- [`.toHaveLabel()`](https://github.com/wix/Detox/blob/master/docs/APIRef.Expect.md#tohavelabellabel)

  ```js
  await expect(element(by.id("UniqueId204"))).toHaveLabel("Done");
  ```

- [`.toHaveId()`](https://github.com/wix/Detox/blob/master/docs/APIRef.Expect.md#tohaveidid)

  ```js
  await expect(element(by.text("I contain some text"))).toHaveId("UniqueId204");
  ```

- [`.toHaveValue()`](https://github.com/wix/Detox/blob/master/docs/APIRef.Expect.md#tohavevaluevalue)

  ```js
  await expect(element(by.id("UniqueId533"))).toHaveValue("0");
  ```

---

## waitFor

```js
await waitFor(element(by.id("UniqueId204")))
  .toBeVisible()
  .withTimeout(2000);
```

```js
await waitFor(element(by.id("UniqueId205")))
  .toBeNotVisible()
  .withTimeout(2000);
```

```js
await waitFor(element(by.id("UniqueId205")))
  .toExist()
  .withTimeout(2000);
```

```js
await waitFor(element(by.id("RandomJunk959")))
  .toNotExist()
  .withTimeout(2000);
```

```js
await waitFor(element(by.id("UniqueId204")))
  .toHaveText("I contain some text")
  .withTimeout(2000);
```

```js
await waitFor(element(by.id("uniqueId")))
  .toHaveValue("Some value")
  .withTimeout(2000);
```

```js
await waitFor(element(by.id("UniqueId336")))
  .toExist()
  .withTimeout(2000);
```

```js
await waitFor(element(by.text("Text5")))
  .toBeVisible()
  .whileElement(by.id("ScrollView630"))
  .scroll(50, "down");
```

---

## Deep Link Test

- Uygulamayı deep link ile açmak

  ```js
  describe("launch app from URL", () => {
    before(async () => {
      await device.launchApp({
        newInstance: true,
        url: "scheme://some.url",
        sourceApp: "com.apple.mobilesafari"
      });
    });

    it("should tap successfully", async () => {
      await expect(element(by.text("a label"))).toBeVisible();
    });
  });
  ```

- Zaten uygulama açık ise

  ```js
  await device.openURL({
    url: "scheme://some.url",
    sourceApp: "com.apple.mobilesafari"
  });
  ```

---

## Notification Test

_[Detaylı bilgi için](https://github.com/wix/Detox/blob/master/docs/APIRef.MockingUserNotifications.md)_

- Uygulamayı notification ile sıfırdan açmak

  ```js
  describe("Background push notification", () => {
    beforeEach(async () => {
      await device.launchApp({
        newInstance: true,
        userNotification: userNotificationPushTrigger
      });
    });

    it("push notification from background", async () => {
      await expect(element(by.text("From push"))).toBeVisible();
    });
  });
  ```

- Zaten uygulama açık ise

  ```js
  describe("Foreground user notifications", () => {
    beforeEach(async () => {
      await device.launchApp();
    });

    it("Local notification from inside the app", async () => {
      await device.sendUserNotification(localNotification);
      await expect(element(by.text("from local notificaton"))).toBeVisible();
    });
  });
  ```
