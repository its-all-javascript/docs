---
menu: Detox
name: Detox Nedir
category: react-native
route: /detox
---

# DETOX NEDİR

Detox, Wix tarafından geliştirilen, mobil uygulamalar için end-to-end test yapmamıza imkân sağlayan framework. Yani uygulamanızı baştan aşağı otomatize edebilirsiniz.

En temel şekilde detox testleri şöyle görünüyor.

```js
describe("Login flow", () => {
  it("should login successfully", async () => {
    await device.reloadReactNative();
    // getting the reference of an element by ID and expecting to be visible
    await expect(element(by.id("email"))).toBeVisible();

    // Getting the reference and typing
    await element(by.id("email")).typeText("john@example.com");
    await element(by.id("password")).typeText("123456");

    // Getting the reference and executing a tap/press
    await element(by.text("Login")).tap();

    await expect(element(by.text("Welcome"))).toBeVisible();
    await expect(element(by.id("email"))).toNotExist();
  });
});
```
