---
menu: Detox
name: Detox Setup IOS
category: react-native
route: /detox-setupi-ios
---

# DETOX SETUP - IOS

## Gereksinimler

- macOS bir bilgisayar (en az macOS High Sierra 10.13.6)

- Xcode 10.1+

## Step 1: Install dependencies

#### 1. [Homebrew](http://brew.sh)

#### 2. [Node.js 8.3.0 ve üstü](https://nodejs.org/en/)

```sh
brew update && brew install node
```

#### 3. [applesimutils](https://github.com/wix/AppleSimulatorUtils)

Apple simulatorler için bir paket.

```sh
brew tap wix/brew
brew install applesimutils
```

> TIP: Yüklendiğini kontrol etmek için `applesimutils` komutunu kullanabilirsiniz

#### 4. Detox command line tools (detox-cli)

```sh
yarn global add detox-cli
```

## Step 2: Detox'u Projeye Ekleme

#### 1. Install detox

```sh
yarn add -D detox
```

#### 2. Detox config'i package.json içine ekle

En temel detox konfigurasyonu aşağıdaki gibi olmalı:

```json
"detox": {
  "configurations": {
    "ios.sim.debug": {
      "binaryPath": "ios/build/Build/Products/Debug-iphonesimulator/scotty.app",
      "build": "xcodebuild -workspace ios/Scotty.xcworkspace -scheme Scotty -configuration Staging -sdk iphonesimulator -derivedDataPath ios/build",
      "type": "ios.simulator",
      "device": {
        "type": "iPhone 11 Pro"
      }
    }
  }
}
```

Eğer projenizde Cocoapods yok ise **"xcodebuild -project ios/Scotty.xcodeproject"**

`applesimutils --list` komutu ile kullanılabilir simulatorleri listeleyebilirsiniz

> TIP: Release versionu test etmek için, 'Debug', 'Release' değişikliğini yapabilirsiniz.

## Step 3: Test Yaz

#### 1. test runner yükle :running_man:

Detox CLI, Jest and Mocha'yı destekliyor. İlk başta birini seçmek zorundasın, ama sonra istersen değiştirebilirsin. (Ama Jest daha komplex ve e2e testler için daha uygun)

[Jest](http://jestjs.io/):

```sh
yarn add -D jest
```

#### 2. test-code dosya yapısını oluştur (automated) :building_construction:

`detox init` komutu zaten bunu sizin için yapıyor.

Jest:

```sh
detox init -r jest
```

**Jest-based environment için şuraya göz atabilirsiniz [Jest setup guide](Guide.Jest.md).**

> `detox init` bizim için ne yapıyor:
>
> - `e2e/` dosyasını projede oluşturuyor
> - `e2e` dosyasının içine `config.json` dosyasını koyuyor.
> - `e2e` dosyasının içine, `init.js` dosyası yaratıyor. Örnek [init.js](/examples/demo-react-native-jest/e2e/init.js).
> - Son olarak `e2e` içine, `firstTest.spec.js` dosyasını yaratıyor. [Şu örnekteki gibi](/examples/demo-react-native/e2e/example.spec.js).

## Step 4: Projeni derle ve Detox testlerini çalıştır

#### 1. Projeyi derlemek

Bunun için aşağıdaki kod kullanılabilir:

```sh
detox build
```

> TIP: Zaten bu `detox build` komutu `package.json` içine yazdığımız konfigurasyona göre çalışıyor.

#### 2. Testleri çalıştır:

```sh
detox test
```

Bu kadar :)

---

## 💀 Olası hatalar

##### applesimutils yükleme sorunu

```sh
 brew install applesimutils
```

komutunu çalıştırdığınızda aşağıdaki hatayı alabilirsiniz.

```sh
Error: applesimutils: /usr/local/Homebrew/Library/Taps/wix/homebrew-brew/applesimutils.rb:4: syntax error, unexpected <<, expecting end
^~
/usr/local/Homebrew/Library/Taps/wix/homebrew-brew/applesimutils.rb:18: syntax error, unexpected ===, expecting end
^~
/usr/local/Homebrew/Library/Taps/wix/homebrew-brew/applesimutils.rb:24: unexpected fraction part after numeric literal
>>>>>>> 0.6.3
```

**Çözüm**:

```sh
 brew untap wix/brew && brew tap wix/brew
```

komutunu çalıştırabilirsiniz. Daha fazla bilgi için [buraya](https://github.com/wix/AppleSimulatorUtils/issues/48#issuecomment-495523750) bakabilirsiniz.

> TIP: node versiyonunuza dikkat edin. Örneğin detox, node 12.6.1 ile çalışmıyor!

---

##### Simulator Cache Problemi

```sh
 for reason: NotFound ("Application "com.usescotty.App.staging" is unknown to FrontBoard").
```

**Çözüm**: Simulatorde, Erase all content and settings ile simülatörünüzün ayarlayını sıfırlayın.

---
