---
menu: Modern JavaScript
name: Modern JavaScript
category: react-native
route: /modern-javascript
---

import { Playground } from 'docz'
import { WebPlayer } from '../../../src/components/WebPlayer.js'

# Modern JavaScript

## Babel

Babel bir **transpiler**. Transpiler, kaynak kodu okur, ve aynı kodu başka bir programlama koduna çevirerek yeni bir çıktı üretir. Babel'in de temel anlamda yaptığı iş; bizim modern javascript dediğimiz EcmaScript ile yazdığımız kodu alıp, okuyup, temel JavaScript'e çevirerek bize tek bir JavaScript bundle'ı üretir. Günün sonunda yazdığımız tüm React Native'deki JavaScript kodu tek bir JS dosyası haline gelir.

------



## Fat Arrow Function ( => )

Fat Arrow Fonksiyonu, anonim fonksiyonlar yaratır. En temel özelliği **this** keyword'unu otomatik olarak bind eder. Transpiler halinde görüldüğü gibi kodu yazarken `var _this = this` gibi bir ifade yazmak zorunda kalmayız.
Fat arrow kullanırken, ilk örnekteki gibi süslü parantez kullanmazsak bu direkt return ifadesi anlamına gelir. Eğer bir şey return etmek istiyorsak, süslü parantez kullandığımızda **return** keyword'u ile bunu belirtmeliyiz.

<Playground>
  <WebPlayer title="Fat Arrow" style={{height:400}} height={400} transpiler code={`const foo = () => 'bar';
const baz = (x) => {
    return x + 1
};
const items = [1,2,3];
items.map((item)=> this.outerFunction(item));`} />
</Playground>

------



## Destructuring

React yazarken Fat Arrow gibi en çok kullanacağımız şeylerden biri de _destructuring_. Birçok key'i olan array veya objelerden ihtiyacımız olanları en pratik biçimde çekebilmemize yardımcı olur.

Örnekleri inceleyecek olursak, ilk örnek elimizdeki array'in ilk elemanını item1 değişkenine, ikini elemanını item2 değişkenine ve geri kalanını da rest değişkenine atıyoruz. rest değişkeni artık 'c' ve 'd' elemanlarından oluşan yeni bir array oluyor.

İkinci örnekte basit bir objeden, istediğimiz key'leri aynı isimde yaratılan değişkenlere set ediyoruz.

Üçüncü örnekte de daha bir iç içe olan nested objemizi, bir önceki örnekte olduğu gibi destruct ediyoruz. Fakat bizim istediğimiz. color ve model key'i, car key'inin altında toplandığı için ilk önce car key'ini destruct edip sonra onun içinden color ve model key'ini alıyoruz. Ayrıca car objesinin olası boş gelme ihtimaline karşılık car key'ini destruct ederken ona boş obje { } ataması yapıyoruz. Çünkü sağ tarafta görüldüğü üzere destruct işlemi aslında objenin içinden başka bir elemana erişme işlemi. car objesi boş olursa undefined.color işlemi uygulamamızın hata vermesine sebep olacaktır.

<Playground>
  <WebPlayer title="Destructuring" style={{height:400}} height={400} transpiler code={`
//array destructuring
const arr = ['a', 'b', 'c', 'd'];
const [item1, item2, ...rest] = arr;
//object destructuring
const object1 = {name: 'Ali', number: '1234', email: 'ali@test.com'};
const {name, number, email} = object1;
//default değer atama
const object2 = { car:{color: 'red', type: 'taxi', model: '2005'} };
const { car: { color, model } = { } } = object2;`}  />
</Playground>

------



## Import ve Export

ES6 ile gelen yeni modüler sistem ile modülleri export ve import etmek daha esnek ve kullanışlı.

Bir modülü export ederken iki seçeneğimiz var ya \_\_esModule olarak default export edebiliriz ya da değişken olarak ismiyle export edebiliriz. Bir dosya içinde sadece bir tane default export tanımlayabiliriz. Ama değişken export'u diye tanımladığım export yöntemiyle birden fazla export işlemi yapabiliriz

<Playground>
  <WebPlayer title="export" style={{height:400}} height={400} transpiler code={`
  export default xyz
  export {a, b, c}; `} />
</Playground>

default export ettiğimiz bir modülü istediğimiz yerden istediğimiz ismi vererek çağırabiliriz. Ama değişken şeklinde export ettiğimiz modül, fonksiyon veya objelerimizi ancak o isimle import edebiliriz. Ya da 4. örnekte olduğu gibi as keyword'u ile alias vererek isimlendirme yapabiliriz.

<Playground>
  <WebPlayer title="import" style={{height:400}} height={400} transpiler code={`
import Abc from 'react';
import { View, Text } from 'react-native';
import XYZ, { a, b, c } from 'xyz';
import React, { Component as MyComponent } from 'react';  `} />
</Playground>

------



## Default Parametre

Bir fonksiyona belli bir parametre verilmez ise default olarak atandığı değere göre çalışması söylenir

<Playground>
  <WebPlayer 
  title="Default Parametre" 
  style={{height:400}} height={400} 
  console = {{"enabled": true, "visible": true, maximized: true, "collapsible": true}} 
  code={`
  const selam = (input = 'Dünya') => {
    console.log('Merhaba ' + input)
  };
  selam()
  selam('Erzurum')`} />
</Playground>

------



## Class

Class yazmak normalde JavaScript'in bir özelliği değil. Ancak EcmaScript2015 ile nesneye yönelimli programlamadan bildiğimiz gibi Class yazmak mümkün.

<Playground>
  <WebPlayer 
  title="Class" 
  style={{height:400}} height={400} 
  console = {{"enabled": true, "visible": true, maximized: true, "collapsible": true}} 
  code={`class HesapMakinesi {
  constructor(input1, input2){
    this.input1 = input1;
    this.input2 = input2;
  } 
  static carp(input1, input2){
    return input1 * input2;
  } 
  topla(){
    return this.input1 + this.input2;
  }
}
console.log(HesapMakinesi.carp(4,2)); //static method olduğu için carp methodunu ancak böyle çağırabiliriz
const Hesaplayıcı = new HesapMakinesi(4, 2); // 4 ve 2 input'larıyla yeni bir class üretiyoruz
console.log(Hesaplayıcı.topla()); // topla methodu Class'ın içindeki this methoduyla verilen parametrelere erişebiliyor
`} />
</Playground>

### Kalıtım (Inheritance)

Inheritance diğer dillerde olduğu gibi extends komutu ile yapılıyor. super methodunu kullanarak parent class'ının özelliklerine erişebiliyoruz. Aşağıdaki örneği inceleyebilirsiniz.

<Playground>
  <WebPlayer 
  title="Class Inheritance" 
  style={{height:400}} height={400} 
  console = {{"enabled": true, "visible": true, maximized: true, "collapsible": true}} 
  code={`class HesapMakinesi {
  constructor(input1, input2){
    this.input1 = input1;
    this.input2 = input2;
  } 
  static carp(input1, input2){
    return input1 * input2;
  } 
  topla(){
    return this.input1 + this.input2;
  }
}
class BenimHesapMakinem extends HesapMakinesi {
  toplaCikar(){
    const toplam = super.topla() - 1;
    return toplam;
  }
}
const hesapMakinem = new BenimHesapMakinem(4,2);
console.log(hesapMakinem.toplaCikar());`} />
</Playground>

------



## Array Spread

Bu operator sayesinde var array'leri teker teker dağıtıp veya bir kaç array'i birleştirebiliriz.

<Playground>
  <WebPlayer 
  title="Array Spread" 
  style={{height:400}} height={400} 
  console = {{"enabled": true, "visible": true, maximized: true, "collapsible": true}} 
  code={`const foo = ['a', 'b', 'c']
const bar = ['d', 'e', 'f']
console.log('foo elemanları', ...foo)
console.log('foobar', [...foo, ...bar])
`} />
</Playground>



------



## Object Spread

Array spread objeleri birleştirmemizi sağlar. Burada dikkat etmemiz gereken mevzu, style objesine olduğu gibi defaultStyle key'lerini dağıtıyoruz. Ancak defaultStyle objesinden farklı olarak, style objesinde fontWeight key değerini değiştiriyoruz. Artı olarak backgroundColor ekliyoruz.

<Playground>
  <WebPlayer 
  title="Object Spread" 
  style={{height:400}} height={400} 
  console = {{"enabled": true, "visible": true, maximized: true, "collapsible": true}} 
  code={`const defaultStyle = {
  color: 'black',
  fontSize: 12,
  fontWeight: 'normal'
}
const style = {
  ...defaultStyle,
  fontWeight: 'bold',
  backgroundColor: 'white'
}
console.log(style)`} />
</Playground>

_Burada anlatılan örnekler [reactnativexpress.com](http://www.reactnativeexpress.com/)'dan faydalanılarak çıkarılmıştır._
