module.exports = {
  siteMetadata: {
    siteUrl: `https://docs.itsalljs.com`
  },
  plugins: [
    "gatsby-theme-docz",
    `gatsby-transformer-sharp`,
    `gatsby-plugin-offline`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `It's All JS Docs`,
        short_name: `its-all-js-docs`,
        start_url: `/`,
        background_color: `#000`,
        theme_color: `#000`,
        display: `standalone`,
        icon: "icons/icon-512x512.png",
        icons: [
          {
            src: `/icons/icon-192x192.png`,
            sizes: `192x192`,
            type: `image/png`
          },
          {
            src: `/icons/icon-512x512.png`,
            sizes: `512x512`,
            type: `image/png`
          }
        ]
      }
    },
    "gatsby-plugin-sitemap",
    {
      resolve: "gatsby-plugin-robots-txt",
      options: {
        host: "https://docs.itsalljs.com",
        sitemap: `https://docs.itsalljs.com/sitemap.xml`,
        policy: [{ userAgent: "*", disallow: "" }]
      }
    }
  ]
};
