import React from "react";

export const ExpoPlayer = () => {
  return (
    <div
      style={{
        overflow: "hidden",
        background: "#212733",
        border: "1px solid rgba(0,0,0,.08)",
        borderRadius: "4px",
        height: "505px"
      }}
    >
      <iframe
        style={{ width: "100%", height: "100%" }}
        src="https://snack.expo.io/embedded/@yosooff/expo-starter?preview=true&platform=web&iframeId=s34v4afd1i&theme=dark"
      />
    </div>
  );
};
