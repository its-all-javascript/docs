import React from "react";
import t from "prop-types";
import { Text, TouchableOpacity } from "react-native-web";

const kinds = {
  info: "#5352ED",
  positive: "#2ED573",
  negative: "#FF4757",
  warning: "#FFA502"
};

const AlertStyled = ({ children, kind, ...rest }) => (
  <TouchableOpacity
    style={{
      padding: 20,
      borderRadius: 3,
      background: kinds[kind]
    }}
    {...rest}
  >
    <Text style={{ color: "white" }}>{children}</Text>
  </TouchableOpacity>
);

export const Alert = props => <AlertStyled {...props} />;

Alert.propTypes = {
  kind: t.oneOf(["info", "positive", "negative", "warning"])
};

Alert.defaultProps = {
  kind: "info"
};
