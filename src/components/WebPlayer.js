import React, { useState } from "react";
import RNWebPlayer from "react-native-web-player";
import {
  View,
  Dimensions,
  TouchableOpacity,
  Text,
  StyleSheet
} from "react-native-web";

const { width } = Dimensions.get("screen");

const mobileWorkspaceCss = `
  @media (max-width: 760px) {
    .CodeMirror pre {
      font-size: 11px;
    } 
  }`;
export const WebPlayer = ({ transpiler, height, ...props }) => {
  const [status, setStatus] = useState("editor");
  const panes = transpiler ? ["editor", "transpiler"] : ["editor", "player"];

  const changePlayer = () => {
    if (status === "editor") {
      setStatus("player");
    } else if (status === "player") {
      setStatus("editor");
    }
  };

  let isMobile = false;
  let _panes = panes;
  if (width < 760) {
    isMobile = true;
  }

  const renderEditor = () => {
    return (
      <>
        <Text style={{ display: "none" }}>Hidden</Text>
        <RNWebPlayer
          transpilerTitle="Babel Output"
          {...props}
          panes={["editor"]}
          workspaceCSS={mobileWorkspaceCss}
        />
      </>
    );
  };

  const renderPlayerTranspiler = pane => {
    let playerStyle = {};
    if (pane === "player") {
      playerStyle = { height: 450 };
    } else {
      playerStyle = { height };
    }

    return (
      <RNWebPlayer
        transpilerTitle="Babel Output"
        {...props}
        style={playerStyle}
        panes={[pane]}
        workspaceCSS={mobileWorkspaceCss}
      />
    );
  };

  const renderMobile = () => {
    const activeStyle = {
      backgroundColor: "#ad51c9"
    };

    const passiveStyle = {
      backgroundColor: "#e1bee7"
    };
    return (
      <>
        <View style={styles.tabContainer}>
          <TouchableOpacity
            onPress={() => setStatus("editor")}
            style={[
              styles.tabButton,
              status === "editor" ? activeStyle : passiveStyle
            ]}
          >
            <Text style={styles.tabText}>editor</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => setStatus(panes[1])}
            style={[
              styles.tabButton,
              status === panes[1] ? activeStyle : passiveStyle
            ]}
          >
            <Text style={styles.tabText}>{panes[1]}</Text>
          </TouchableOpacity>
        </View>
        {status === "editor"
          ? renderEditor()
          : renderPlayerTranspiler(panes[1])}
      </>
    );
  };

  return (
    <View style={{ flex: 1 }}>
      {isMobile ? (
        renderMobile()
      ) : (
        <RNWebPlayer transpilerTitle="Babel Output" {...props} panes={panes} />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  tabContainer: {
    height: 40,
    flex: 1,
    position: "relative",
    marginBottom: 5,
    flexDirection: "row",
    justifyContent: "space-around"
  },
  tabButton: {
    flex: 1,
    paddingVertical: 7
  },
  tabText: {
    textAlign: "center",
    fontWeight: "bold",
    color: "#fff"
  }
});
