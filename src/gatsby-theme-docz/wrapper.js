// src/gatsby-theme-docz/index.js
import React from "react";
import { useCurrentDoc } from "docz";
import { globalStyle } from "./GlobalStye";
import { HomePage } from "./HomePage";
import { Global } from "@emotion/core";

const Layout = ({ children }) => {
  const currentDoc = useCurrentDoc();
  return (
    <>
      <Global styles={globalStyle} />
      {currentDoc.route !== "/" ? children : <HomePage />}
    </>
  );
};

export default Layout;
