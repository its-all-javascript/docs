import React from "react";
import {
  Box,
  Grid,
  Container,
  Flex,
  Text,
  Heading,
  Link
} from "@theme-ui/components";
import { css } from "@emotion/core";
import { Header } from "./components/Header";
import { ContentCard } from "./components/ContentCard";

const ReactNativeIcon = require("./assets/images/reactnative.png");
const JavaScriptIcon = require("./assets/images/javascript.png");
const ApolloIcon = require("./assets/images/apollo.png");
export const HomePage = () => {
  return (
    <>
      <Header />
      <Container css={containerStyle} p={4}>
        <Grid gap={50} columns={[1, null, 2]}>
          <Grid gap={20} columns={[1, 2, 2]}>
            <ContentCard
              icon={ReactNativeIcon}
              label="React Native"
              to="/react-native"
            />
            <ContentCard
              icon={ReactNativeIcon}
              label="React JS"
              ready={false}
            />
            <ContentCard
              icon={JavaScriptIcon}
              label="Javascript"
              to="/javascript"
            />
            <ContentCard
              icon={ApolloIcon}
              label="Apollo Graphql"
              ready={false}
            />
          </Grid>
          <Box>
            <Heading>itsalljs Dökümanları</Heading>
            <Text>
              Blog sitemizden farklı olarak, belli başlı kütüphanelerin veya
              dillerin konularını sırasıyla derlediğimiz dökümanlar. Bunları
              e-kitap olarak düşünebilirsiniz. Bu dökümanlara katkıda bulunmak
              isterseniz,{" "}
              <Link href="https://gitlab.com/its-all-javascript/docs">
                buraya
              </Link>{" "}
              tıklayarak düzenleyebilirsiniz
            </Text>
          </Box>
        </Grid>
      </Container>
    </>
  );
};

const containerStyle = css`
  min-height: 100vh;
`;
