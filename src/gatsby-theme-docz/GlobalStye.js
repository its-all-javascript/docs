import { css } from "@emotion/core";

export const globalStyle = css`
  html,
  body {
    height: 100%;
    margin: 0;
    padding: 0;
  }

  pre {
    overflow: auto;
  }

  @media (max-width: 921px) {
    h1 {
      font-size: 24px !important;
    }

    h2 {
      font-size: 21px !important;
    }

    pre {
      margin-left: -32px !important;
      margin-right: -32px !important;
      border-radius: 0 !important;
    }
  }

  table {
    border: 1px solid #ccc;
    width: 100%;
    margin: 0;
    padding: 0;
    border-collapse: collapse;
    border-spacing: 0;
  }

  table tr {
    border: 1px solid #ddd;
    padding: 5px;
  }

  table th,
  table td {
    padding: 10px;
    text-align: center;
    border: 0.5px solid #ccc;
  }

  table th {
    text-transform: uppercase;
    font-size: 14px;
    letter-spacing: 1px;
  }

  @media screen and (max-width: 600px) {
    table {
      border: 0;
    }

    table thead {
      display: none;
    }

    table tr {
      margin-bottom: 10px;
      display: block;
      border-bottom: 2px solid #ddd;
    }

    table td {
      display: block;
      text-align: right;
      font-size: 13px;
      border-bottom: 1px dotted #ccc;
    }

    table td:last-child {
      border-bottom: 0;
    }

    table td:before {
      content: attr(data-label);
      float: left;
      text-transform: uppercase;
      font-weight: bold;
    }
  }
`;
