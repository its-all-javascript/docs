import React from "react";
import { Box, Image, Text, Card } from "@theme-ui/components";
import { useThemeUI } from "theme-ui";
import { css } from "@emotion/core";
import { Link } from "docz";
export const ContentCard = ({ icon, label, ready = true, to }) => {
  const context = useThemeUI();
  const { theme, colorMode } = context;
  const bg = colorMode === "light" ? "#fff" : theme.colors.secondary;
  const color = colorMode === "light" ? theme.colors.text : "#fff";
  return (
    <Link to={to} css={link} style={{ color }}>
      <Card bg={bg} css={container}>
        <Image src={icon} css={image} />
        <Text>{label}</Text>
        {!ready && <Text>(Yakında)</Text>}
      </Card>
    </Link>
  );
};

const container = css`
  height: 184px;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  border-radius: 6px;
  cursor: pointer;
  transition: all 0.3s ease-in-out;
  border: 1px solid #e0e0e0;

  :hover {
    transform: scale(1.1);
  }

  :active {
    transform: scale(1);
  }
`;

const image = css`
  width: 40px;
  height: auto;
  margin-bottom: 15px;
`;

const link = css`
  text-decoration: none;
`;
