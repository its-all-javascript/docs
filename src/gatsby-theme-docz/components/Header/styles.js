import { media } from "../../breakpoints";
import { css } from "@emotion/core";

const mixins = {
  centerAlign: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  ghostButton: {
    p: 0,
    outline: "none",
    background: "transparent",
    border: "none",
    ":hover": {
      cursor: "pointer"
    }
  }
};

export const wrapper = {
  bg: "header.bg",
  position: "relative",
  zIndex: 1,

  background: "transparent",
  height: 88,
  display: "flex",

  justifyContent: "space-between",
  flexDirection: "row",
  alignItems: "center",
  px: 4
};

export const menuIcon = {
  visibility: "hidden",
  top: "calc(100% + 15px)",
  left: 30,
  [media.tablet]: {
    visibility: "visible"
  }
};

export const menuButton = {
  ...mixins.ghostButton,
  color: "header.text",
  opacity: 0.5,
  cursor: "pointer"
};

export const headerButton = {
  ...mixins.centerAlign,
  outline: "none",
  p: "12px",
  border: "none",
  borderRadius: 9999,
  bg: "header.button.bg",
  color: "header.button.color",
  fontSize: 0,
  fontWeight: 600,
  cursor: "pointer"
};

export const editButton = {
  ...mixins.centerAlign,
  position: "absolute",
  bottom: -40,
  right: 30,
  bg: "transparent",
  color: "muted",
  fontSize: 1,
  textDecoration: "none",
  borderRadius: "radius"
};
