/** @jsx jsx */
import { jsx, Box, Flex, useColorMode } from "theme-ui";
import { useConfig, useCurrentDoc } from "docz";
import * as styles from "./styles";
import {
  Edit,
  Menu,
  Sun,
  Github
} from "gatsby-theme-docz/src/components/Icons";
import { Logo } from "../Sidebar/Logo";

export const Header = props => {
  const { onOpen } = props;
  const {
    repository,
    themeConfig: { showDarkModeSwitch }
  } = useConfig();
  const { edit = true, ...doc } = useCurrentDoc();
  const [colorMode, setColorMode] = useColorMode();

  const toggleColorMode = () => {
    setColorMode(colorMode === "light" ? "dark" : "light");
  };

  return (
    <div sx={styles.wrapper} data-testid="header">
      {doc.route !== "/" ? (
        <Box sx={styles.menuIcon}>
          <button sx={styles.menuButton} onClick={onOpen}>
            <Menu size={25} />
          </button>
        </Box>
      ) : (
        <Logo />
      )}
      {showDarkModeSwitch && (
        <button sx={styles.headerButton} onClick={toggleColorMode}>
          <Sun size={15} />
        </button>
      )}
    </div>
  );
};
