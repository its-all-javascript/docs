/** @jsx jsx */
import { useRef, useState } from "react";
import {
  jsx,
  Layout as BaseLayout,
  Main,
  Container,
  useColorMode
} from "theme-ui";
import { Global, css } from "@emotion/core";

import { Header } from "../Header";
import { Sidebar } from "../Sidebar";
import * as styles from "./styles";

export const Layout = ({ children }) => {
  const [open, setOpen] = useState(false);
  const [colorMode, setColorMode] = useColorMode();
  const nav = useRef();

  const contentBackground = colorMode === "light" ? "white" : "#1c1f2c";

  return (
    <BaseLayout style={{ flexDirection: "row" }} data-testid="layout">
      <Sidebar
        ref={nav}
        open={open}
        onFocus={() => setOpen(true)}
        onBlur={() => setOpen(false)}
        onClick={() => setOpen(false)}
      />
      <div css={content} style={{ background: contentBackground }}>
        <Header onOpen={() => setOpen(s => !s)} />
        <Container css={mainContent} data-testid="main-container">
          {children}
        </Container>
      </div>
    </BaseLayout>
  );
};

const content = css`
  width: calc(100% - 250px);
  @media (max-width: 921px) {
    width: 100%;
  }
`;

const mainContent = css`
  max-width: 770px;
`;
