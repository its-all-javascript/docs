import { media } from "../../breakpoints";

export const main = {
  display: "flex",
  flexDirection: "row",
  minHeight: "100vh"
};
