import reactNativeRoutes from "./routes-react-native";
import javascriptRoutes from "./routes-javascript";
export default {
  title: "itsalljs",
  ignore: ["**/src/**", "README.md"],
  menu: [...javascriptRoutes, ...reactNativeRoutes],
  themeConfig: {
    mode: "light",
    showPlaygroundEditor: false,

    styles: {
      h1: {
        fontFamily: "Roboto, Helvetica, Arial, sans-serif"
      },
      h2: {
        fontFamily: "Roboto, Helvetica, Arial, sans-serif"
      },
      h3: {
        fontFamily: "Roboto, Helvetica, Arial, sans-serif"
      },
      p: {
        fontFamily: "Roboto, Helvetica, Arial, sans-serif"
      },
      div: {
        fontFamily: "Roboto, Helvetica, Arial, sans-serif"
      },
      a: {
        fontFamily: "Roboto, Helvetica, Arial, sans-serif"
      },
      li: {
        fontFamily: "Roboto, Helvetica, Arial, sans-serif"
      }
    },
    prismTheme: {
      plain: {
        color: "#9EFEFF",
        backgroundColor: "#2D2A55"
      },
      styles: [
        {
          types: ["changed"],
          style: {
            color: "rgb(255, 238, 128)"
          }
        },
        {
          types: ["deleted"],
          style: {
            color: "rgba(239, 83, 80, 0.56)"
          }
        },
        {
          types: ["inserted"],
          style: {
            color: "rgb(173, 219, 103)"
          }
        },
        {
          types: ["comment"],
          style: {
            color: "rgb(179, 98, 255)",
            fontStyle: "italic"
          }
        },
        {
          types: ["punctuation"],
          style: {
            color: "rgb(255, 255, 255)"
          }
        },
        {
          types: ["constant"],
          style: {
            color: "rgb(255, 98, 140)"
          }
        },
        {
          types: ["string", "url"],
          style: {
            color: "rgb(165, 255, 144)"
          }
        },
        {
          types: ["variable"],
          style: {
            color: "rgb(255, 238, 128)"
          }
        },
        {
          types: ["number", "boolean"],
          style: {
            color: "rgb(255, 98, 140)"
          }
        },
        {
          types: ["attr-name"],
          style: {
            color: "rgb(255, 180, 84)"
          }
        },
        {
          types: [
            "keyword",
            "operator",
            "property",
            "namespace",
            "tag",
            "selector",
            "doctype"
          ],
          style: {
            color: "rgb(255, 157, 0)"
          }
        },
        {
          types: ["builtin", "char", "constant", "function", "class-name"],
          style: {
            color: "rgb(250, 208, 0)"
          }
        }
      ]
    }
  }
};
