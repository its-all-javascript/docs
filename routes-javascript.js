const routes = [
  {
    name: "Programlama Temelleri",
    category: "javascript",
    menu: [
      {
        name: "Kodlama",
        category: "javascript"
      }
    ]
  },
  {
    name: "JavaScript Temelleri",
    category: "javascript",
    menu: [
      {
        name: "Types",
        category: "javascript"
      },
      {
        name: "Coercion",
        category: "javascript"
      },
      {
        name: "True/False",
        category: "javascript"
      },
      {
        name: "Eşitlik",
        category: "javascript"
      }
    ]
  }
];

export default routes;
