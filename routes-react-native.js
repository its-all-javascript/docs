const routes = [
  {
    name: "Başlangıç",
    category: "react-native",
    menu: [
      {
        name: "React Native Nedir",
        category: "react-native"
      }
    ]
  },
  {
    name: "Modern JavaScript",
    category: "react-native"
  },
  {
    name: "Kurulum",
    category: "react-native",
    menu: [
      {
        name: "Windows Kurulumu",
        category: "react-native"
      },
      {
        name: "MacOS Kurulumu",
        category: "react-native"
      },
      {
        name: "GNU/Linux Kurulumu",
        category: "react-native"
      },
      {
        name: "Expo",
        category: "react-native"
      }
    ]
  },
  {
    name: "Style",
    category: "react-native",
    menu: [
      {
        name: "FlexBox Style",
        category: "react-native"
      }
    ]
  },
  {
    name: "Detox",
    category: "react-native",
    menu: [
      {
        name: "Detox Nedir",
        category: "react-native"
      },
      {
        name: "Detox Setup IOS",
        category: "react-native"
      },
      {
        name: "Detox Setup Android",
        category: "react-native"
      },
      {
        name: "Detox API",
        category: "react-native"
      }
    ]
  }
];

export default routes;
